package info.hccis.ojt.web;

import info.hccis.admin.dao.StudentDAO;
import info.hccis.ojt.data.springdatajpa.UserRepository;
import info.hccis.camper.model.DatabaseConnection;
import info.hccis.camper.model.Student;
import info.hccis.camper.model.jpa.CodeValue;
import info.hccis.camper.model.jpa.User;
import info.hccis.camper.util.Utility;
import info.hccis.ojt.data.springdatajpa.StudentRepository;
import info.hccis.user.bo.UserBO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StudentController {

    private final StudentRepository sr;

    /**
     * This constructor will inject the UserRepository object into this
     * controller. It will allow this class methods to use methods from this
     * Spring JPA repository object.
     *
     * @since 20180524
     * @author BJM
     * @param ur
     */
    @Autowired
    public StudentController(StudentRepository sr) {
        this.sr = sr;
    }

    @RequestMapping("/student/studentList")
    public String showStudent(Model model) {

        //Get the ojt from the database
        model.addAttribute("students", sr.findAll());
        
        //This will send the user to the Studentlist.html page.
        return "student/studentList";
    }
    
    @RequestMapping("/student/studentListResume")
    public String showStudentResume(Model model) {
        
        List<Student> listEdit = new ArrayList<Student>();
        
        listEdit=sr.findByResumeSubmittedDateIsNotNullAndCoverLetterSubmittedDateIsNotNull();
        
        Iterator<Student> i = listEdit.iterator();
        while (i.hasNext()) {
           Student s = i.next(); // must be called before you can call i.remove()
           // Do something
           if (s.getResumeSubmittedDate().isEmpty()) {
               i.remove();
           }  
           else if(s.getCoverLetterSubmittedDate().isEmpty())
           {
                i.remove();
           }
        }


        //Get the ojt from the database
        model.addAttribute("students",listEdit);
        
        //This will send the user to the StudentList.html page.
        return "student/studentList";
    }
    
    @RequestMapping("/student/import")
    public String importStudent(Model model) {

        //Get the ojt from the database
        StudentDAO.insertCSV();
        
        model.addAttribute("students", sr.findAll());
        //This will send the user to the Studentlist.html page.
        return "student/studentList";
    }
    
    @RequestMapping("/student/update")
    public String ojtStudentUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);
 

       //Use Spring data JPA instead
        Student editStudent = sr.findOne(Integer.parseInt(idToFind));
//
        model.addAttribute("student", editStudent);

        return "student/add";
    }
    
    @RequestMapping("/student/add")
    public String ojtStudentAdd(Model model) {


        Student student = new Student();
        model.addAttribute("student", student);
        return "student/add";
    }
    
     @RequestMapping("/student/addSubmit")
    public String ojtStudentAddSubmit(Model model,@Valid @ModelAttribute("student") Student theStudentFromForm,BindingResult result) {


        if (Utility.TESTING) {
            System.out.println("BJM-Checking validation." + result.getErrorCount());
        }
        
        if (result.hasErrors()) {
            System.out.println("Error in validation." + result.toString());
            model.addAttribute("student", theStudentFromForm);
            return "student/add";
        }
        //Call the dao method to put this guy in the database.
        try {
            sr.save(theStudentFromForm);
            //OjtDAO.update(theOjtFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("BJM-Did we get here?");
        //Reload the ojtReflections list so it can be shown on the next view.
//        model.addAttribute("ojtReflections", OjtDAO.selectAll());
        //This will send the user to the welcome.html page.
        
        
        long size = sr.count();
        //model.addAttribute("message", "ojtReflections updated.  There are now "+size+" ojtReflections");
        model.addAttribute("students", sr.findAll());
        
        return "student/studentList";
    }
    
   @RequestMapping("/student/delete")
   public String ojtStudentDelete(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);
//        try {
//            //- It will go to the database and load the OBJ details into a OBJ
//            //  object and put that object in the model.
//            OjtDAO.delete(Integer.parseInt(idToFind));
//        } catch (Exception ex) {
//            System.out.println("Could not delete");
//        }

        sr.delete(Integer.parseInt(idToFind));

        model.addAttribute("students", sr.findAll());

        //Reload the ojtReflection list so it can be shown on the next view.
        //model.addAttribute("ojtReflections", OjtDAO.selectAll());
        return "/student/studentList";
    }
        


}
