package info.hccis.ojt.web;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import info.hccis.admin.dao.PlacementDAO;
import info.hccis.admin.dao.StudentDAO;
import info.hccis.camper.model.Student;
import info.hccis.camper.model.jpa.Placement;
import info.hccis.camper.model.jpa.PlacementExtended;
import info.hccis.camper.util.Utility;
import info.hccis.ojt.data.springdatajpa.BusinessRepository;
import info.hccis.ojt.data.springdatajpa.PlacementRepository;
import info.hccis.ojt.data.springdatajpa.StudentRepository;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This controller will write the data from database into a text file with
 * current date
 *
 * @author eateeque
 * @since 20181024
 */
@Controller
public class PlacementController {

    private final PlacementRepository pr;
    private final StudentRepository sr;
    private final BusinessRepository br;

    /**
     * This constructor will inject the PlacementRepository object into this
     * controller. It will allow this class methods to use methods from this
     * Spring JPA repository object.
     *
     * @since 20180524
     * @author erumateeque
     * @param pr
     */
    @Autowired
    public PlacementController(PlacementRepository pr, StudentRepository sr, BusinessRepository br) {
        this.pr = pr;
        this.sr = sr;
        this.br = br;

    }

    @RequestMapping("/placement/export")
    public String WritePlacements(Model model) throws IOException {

        System.out.println("Did we get here?");

        // Get a Calendar and set it to the current time.
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(Instant.now()));

        String result = String.format(
                "ojt_%1$tY%1$tm%1$td.txt", cal);

        //create a file with current date
        File file = new File("/cis2232", result);
        FileWriter fr = null;
        BufferedWriter br = null;

        try {
            fr = new FileWriter(file);
            br = new BufferedWriter(fr);

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String reflectionAsJson = gson.toJson(PlacementDAO.selectAll());
            br.write(reflectionAsJson + System.lineSeparator());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "placement/export";
    }

    @RequestMapping("/placement/list")
    public String showPlacement(Model model) {

        //Get the placement from the database
        
        model.addAttribute("placementList", PlacementDAO.selectAll());

        //This will send the user to the list page
        return "placement/list";

    }

    @RequestMapping("/placement/studentReport")
    public String showStudents(Model model, HttpServletRequest request) {

        //Get the placement from the database
       
        model.addAttribute("studentsList", PlacementDAO.selectStudentNotWithPlacement());

        //This will send the user to the student report page
        return "placement/studentReport";

    }

    @RequestMapping("/placement/businessReport")
    public String showBusinesses(Model model, HttpServletRequest request) {

        //Get the placement from the database
       
        model.addAttribute("businessesList", PlacementDAO.selectBusinessWithNoPlacement());

        //This will send the user to the business report page
        return "placement/businessReport";

    }

    @RequestMapping("/placement/add")
    public String placementAdd(Model model) {

        //put a placement object in the model to be used to associate with the input tags of 
        //the form.
        model.addAttribute("students", sr.findAll());
        model.addAttribute("businesses", br.findAll());

        Placement newPlacement = new Placement();
        newPlacement.setId(0);
        model.addAttribute("placement", newPlacement);

        return "placement/add";
    }

    @RequestMapping("/placement/update")
    public String studentUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("ERM id passed=" + idToFind);
        //- It will go to the database and load the placement details into a placement
        //  object and put that object in the model.  
        //*******************************************************************
        //Use Spring data JPA 
        model.addAttribute("students", sr.findAll());
        model.addAttribute("businesses", br.findAll());
        Placement editPlacement = pr.findOne(Integer.parseInt(idToFind));

        model.addAttribute("placement", editPlacement);
        return "/placement/add";

    }

    @RequestMapping("/placement/delete")
    public String studentDelete(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("ERM id passed=" + idToFind);
        try {
            //- It will go to the database and load the placement details into a placement
            //  object and put that object in the model.
           
            pr.delete(Integer.parseInt(idToFind));
        } catch (Exception ex) {
            System.out.println("Could not delete");
        }

        //Reload the placement list so it can be shown on the next view.
        model.addAttribute("placementList", PlacementDAO.selectAll());
        return "/placement/delete";

    }

    @RequestMapping("/placement/addSubmit")
    public String placementAddSubmit(Model model, @Valid @ModelAttribute("placement") Placement thePlacementFromTheForm, BindingResult result) {

        if (Utility.TESTING) {
            System.out.println("Checking validation." + result.getErrorCount());
        }

        if (result.hasErrors()) {
            System.out.println("Error in validation." + result.toString());
            //  model.addAttribute("message","Error");
        model.addAttribute("students", sr.findAll());
        model.addAttribute("businesses", br.findAll());
            return "/placement/add";
        }
       

        //Call the dao method to put this guy in the database.
        try {

            pr.save(thePlacementFromTheForm);

        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Could not save to the database");
        }

        System.out.println("Did we get here?");
        //Reload the student list so it can be shown on the next view.
        long size = pr.count();
        model.addAttribute("message", "Placement updated.  There are now " + size + " Placements");
        model.addAttribute("placementList", PlacementDAO.selectAll());
        //This will send the user to the list.html page.
        return "placement/list";
    }

}
