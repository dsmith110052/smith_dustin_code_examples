/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.data.springdatajpa;

import info.hccis.camper.model.Business;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author HomeUser
 */
public interface BusinessRepository extends CrudRepository<Business, Integer>{
    List<Business> findByName(String Name);

}
    