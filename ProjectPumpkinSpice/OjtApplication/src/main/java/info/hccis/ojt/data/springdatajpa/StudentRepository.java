/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.data.springdatajpa;

import info.hccis.camper.model.Student;
import java.util.List;
import java.util.stream.Stream;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author HomeUser
 */
public interface StudentRepository extends CrudRepository<Student, Integer>{
//    List<Student> findByLastName(String lastName);
//    List<Student> findByDob(String dob);
    List<Student> findByUsername(String Username);
    List<Student> findByCoverLetterSubmittedDateAndResumeSubmittedDate(String coverLetterSubmittedDate, String resumeSubmittedDate);
    List<Student> findByResumeSubmittedDateIsNotNullAndCoverLetterSubmittedDateIsNotNull();
    List<Student> findByResumeSubmittedDateLikeAndCoverLetterSubmittedDateLike(String resumeSubmittedDate, String coverLetterSubmittedDate);

//    @Query("from Auction a join a.category c where c.name=:categoryName")
//    Iterable<Student> findByCategory();
    

}
