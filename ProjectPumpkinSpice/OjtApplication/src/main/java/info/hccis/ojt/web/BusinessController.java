package info.hccis.ojt.web;

import info.hccis.admin.dao.BusinessDAO;
import info.hccis.camper.model.Business;
import info.hccis.camper.util.Utility;
import info.hccis.ojt.data.springdatajpa.BusinessRepository;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BusinessController {

    private final BusinessRepository br;

    
    @Autowired
    public BusinessController(BusinessRepository br) {
        this.br = br;
    }
    
    @RequestMapping("/business/import")
    public String importBusiness(Model model) {
        BusinessDAO.insert();
        return "/business/import";
    }
    
    @RequestMapping("/business/show")
    public String showStudent(Model model) {

        model.addAttribute("businesses", br.findAll());
        return "/business/show";
    }
    
    @RequestMapping("/business/add")
    public String businessAdd(Model model) {

        Business newBusiness = new Business();
        model.addAttribute("business", newBusiness);

        return "/business/add";
    }

    @RequestMapping("/business/update")
    public String businessUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);
        
        Business editBusiness = br.findOne(Integer.parseInt(idToFind));

        model.addAttribute("business", editBusiness);
        return "/business/add";

    }

  

    @RequestMapping("business/delete")
    public String businessDelete(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);

        br.delete(Integer.parseInt(idToFind));

        model.addAttribute("businesses", br.findAll());
        return "/business/list";

    }
    @RequestMapping("/business/addSubmit")
    public String BusinessAddSubmit(Model model, @Valid @ModelAttribute("business") Business theBusinessFromTheForm, BindingResult result) {

        if (Utility.TESTING) {
            System.out.println("BJM-Checking validation." + result.getErrorCount());
        }

        if (result.hasErrors()) {
            System.out.println("Error in validation."+ result.toString());
            model.addAttribute("message", theBusinessFromTheForm);

            return "/business/add";
        }

        try {

            br.save(theBusinessFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("BJM-Did we get here?");
        
        
        long size = br.count();
        model.addAttribute("message", "Business updated.  There are now "+size+" businesses");
        model.addAttribute("businesses", br.findAll());
        return "/business/show";
    }
}