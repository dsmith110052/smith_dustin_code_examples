package info.hccis.camper.rest;

import com.google.gson.Gson;
import info.hccis.ojt.data.springdatajpa.BusinessRepository;
import info.hccis.camper.model.Business;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/BusinessService")
public class BusinessService {

    @Resource
    private final BusinessRepository br;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * OjtRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20181109
     * @author BJM
     */
    public BusinessService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.br = applicationContext.getBean(BusinessRepository.class);
    }

    /**
     * This rest service will provide all users from the associated database.
     *
     * @return json string containing all user information.
     * @since 20181109
     * @author BJM
     */
    
    @GET
    @Path("/business")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {

        ArrayList<Business> theOjt = (ArrayList<Business>) br.findAll();
        Gson gson = new Gson();
        int statusCode = 200;

        if (theOjt.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(theOjt);

        return Response.status(statusCode)
                .entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
    @GET
    @Path("/business/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@PathParam("name") String Name) {

        ArrayList<Business> theOjt = (ArrayList<Business>) br.findByName(Name);
        Gson gson = new Gson();
        int statusCode = 200;

        if (theOjt.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(theOjt);

        return Response.status(statusCode)
                .entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
}



