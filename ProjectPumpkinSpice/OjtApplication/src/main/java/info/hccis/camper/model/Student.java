/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.camper.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author banderson30742
 */
@Entity
@Table(name = "student")
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s")})
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "username")
    private String username;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 15)
    @Column(name = "phone")
    private String phone;
    @Size(max = 50)
    @Column(name = "studentId")
    private String studentId;
    @Size(max = 50)
    @Column(name = "lastName")
    private String lastName;
    @Size(max = 20)
    @Column(name = "firstName")
    private String firstName;
    @Size(max = 100)
    @Column(name = "resumeLocation")
    private String resumeLocation;
    @Size(max = 140)
    @Column(name = "preferredTypeOfWork")
    private String preferredTypeOfWork;
    @Size(max = 10)
    @Column(name = "coverLetterSubmittedDate")
    private String coverLetterSubmittedDate;
    @Size(max = 10)
    @Column(name = "resumeSubmittedDate")
    private String resumeSubmittedDate;

    public Student() {
    }

    public Student(Integer id) {
        this.id = id;
    }

    public Student(Integer id, String username, String phone, String studentId, String lastName, String firstName, String resumeLocation, String preferredTypeOfWork, String coverLetterSubmittedDate, String resumeSubmittedDate) {
        this.id = id;
        this.username = username;
        this.phone = phone;
        this.studentId = studentId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.resumeLocation = resumeLocation;
        this.preferredTypeOfWork = preferredTypeOfWork;
        this.coverLetterSubmittedDate = coverLetterSubmittedDate;
        this.resumeSubmittedDate = resumeSubmittedDate;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getResumeLocation() {
        return resumeLocation;
    }

    public void setResumeLocation(String resumeLocation) {
        this.resumeLocation = resumeLocation;
    }

    public String getPreferredTypeOfWork() {
        return preferredTypeOfWork;
    }

    public void setPreferredTypeOfWork(String preferredTypeOfWork) {
        this.preferredTypeOfWork = preferredTypeOfWork;
    }

    public String getCoverLetterSubmittedDate() {
        return coverLetterSubmittedDate;
    }

    public void setCoverLetterSubmittedDate(String coverLetterSubmittedDate) {
            this.coverLetterSubmittedDate = coverLetterSubmittedDate;
    }

    public String getResumeSubmittedDate() {
        return resumeSubmittedDate;
    }

    public void setResumeSubmittedDate(String resumeSubmittedDate) {
        this.resumeSubmittedDate = resumeSubmittedDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.camper.model.Student[ id=" + id + " ]";
    }
    
}
