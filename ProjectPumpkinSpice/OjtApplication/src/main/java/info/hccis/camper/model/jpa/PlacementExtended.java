/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.camper.model.jpa;

/**
 *
 * @author eateeque
 */
public class PlacementExtended extends Placement{
    
    private String firstName;

    
    private String lastName;
    private String businessName;
    
    public PlacementExtended(Integer id) {
        super(id);
    }
    
    

    public PlacementExtended() {
       
    }

    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }
    @Override
    public String toString(){
        
        return "First Name= "+firstName+ " Last Name = "+lastName + "Bussiness Name = "+businessName;
    }
}
