
package info.hccis.camper.rest;

import com.google.gson.Gson;
import info.hccis.admin.dao.PlacementDAO;
import info.hccis.camper.model.Business;
import info.hccis.camper.model.jpa.PlacementExtended;
import info.hccis.ojt.data.springdatajpa.PlacementRepository;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author erumAteeque
 * @since 20181130
 */
@Path("/PlacementService")
public class PlacementService {

    @Resource
    private final PlacementRepository pr;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * PlacementRepository object for use in this class.
     *
     * @param servletContext Context
     * @author erumAteeque
     * @since 20181130
     */
    public PlacementService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.pr = applicationContext.getBean(PlacementRepository.class);
    }

    /**
     * This rest service will provide all business that still needs Ojt
     * students
     *
     * @return json string containing all user information.
     * @author erumAteeque
     * @since 20181130
     */
    @GET
    @Path("/businessNeedStudents")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBusiness() {

        ArrayList<Business> theBusiness = (ArrayList<Business>) PlacementDAO.selectBusinessWithNoPlacement();
        Gson gson = new Gson();
        int statusCode = 200;

        if (theBusiness.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(theBusiness);

        return Response.status(statusCode)
                .entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

    /**
     * This rest service will provide all placements from the the file.
     *
     * @return json string containing all user information.
     * @author erumAteeque
     * @since 20181130
     */
    @GET
    @Path("/placements")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPalcement() {

        ArrayList<PlacementExtended> thePlacement = (ArrayList<PlacementExtended>) PlacementDAO.selectAll();
        Gson gson = new Gson();
        int statusCode = 200;

        if (thePlacement.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(thePlacement);

        return Response.status(statusCode)
                .entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

}
