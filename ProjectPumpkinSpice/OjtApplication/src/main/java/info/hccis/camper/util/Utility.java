package info.hccis.camper.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.*;
import java.math.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * General program utilities
 *
 * @author bjmaclean
 * @since 20150918
 */
public class Utility {

    //Used to turn on/off additonal sout in application.
    public static final boolean DEBUGGING = true;
    public static boolean TESTING = true;

    public static String getMessage(String key) {
        String propFileName = "messages.messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        return rb.getString(key);
    }

    public static String getMessage(String key, String parameter1) {
        String message = getMessage(key);
        return MessageFormat.format(message, parameter1);
    }

    public static String getCSV(String[] test) {
        String output = "";
        boolean found = false;
        for (String current : test) {
            found = true;
            output += current + ",";
        }
        output = output.substring(0, output.length());
        return output;
    }

    public static String getCSV(int[] test) {
        String output = "";
        boolean found = false;
        for (int current : test) {
            found = true;
            output += "" + current + ",";
        }
        output = output.substring(0, output.length());
        return output;
    }

    /**
     * This method will use md5 to create a hash value for the pw.
     *
     * @since 20170516
     * @author BJM
     */
    public static String getHashPassword(String inPassword) {
        try {
            return getMD5Hash(inPassword);
        } catch (Exception ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Should not see this!";
    }

    public static String getMD5Hash(String passwordIn) throws Exception {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(passwordIn.getBytes(), 0, passwordIn.length());
        return "" + new BigInteger(1, m.digest()).toString(16);
    }

    /**
     * This wil;l give current date/time in format provided
     *
     * @since 20170516
     * @param format
     * @return formatted now
     */
    public static String getNow(String format) {
        if (format.isEmpty()) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        DateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();
        String now = dateFormat.format(date);
        return now;

    }

    public static String getJsonForRest(String urlIn) {
        String output = "";
        try {

            URL url = new URL(urlIn);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            System.out.println("Output from Server .... \n");
            String outputPart;
            while ((outputPart = br.readLine()) != null) {
                output += outputPart;
            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("JSON back from web service:");
        return output;
    }

}
