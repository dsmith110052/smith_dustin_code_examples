package info.hccis.camper.rest;

import com.google.gson.Gson;
import info.hccis.ojt.data.springdatajpa.StudentRepository;
import info.hccis.camper.model.Student;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/StudentService")
public class StudentService {

    @Resource
    private final StudentRepository cr;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * OjtRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20181109
     * @author BJM
     */
    public StudentService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cr = applicationContext.getBean(StudentRepository.class);
    }

    /**
     * This rest service will provide all users from the associated database.
     *
     * @return json string containing all user information.
     * @since 20181109
     * @author BJM
     */
    @GET
    @Path("/student")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {

        ArrayList<Student> theOjt = (ArrayList<Student>) cr.findAll();
        Gson gson = new Gson();
        int statusCode = 200;

        if (theOjt.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(theOjt);

        return Response.status(statusCode)
                .entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }
    
    @GET
    @Path("/student/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@PathParam("username") String Username) {

        ArrayList<Student> theOjt = (ArrayList<Student>) cr.findByUsername(Username);
        Gson gson = new Gson();
        int statusCode = 200;

        if (theOjt.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(theOjt);

        return Response.status(statusCode)
                .entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

    
    
    /**
     * POST operation which will update a Reflection.
     * @param jsonIn 
     * @return response including the json representing the new Reflection.
     * @throws IOException 
     */
    
//    @POST
//    @Path("/student")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response updateReflection(String jsonIn) throws IOException {
//
//////        ObjectMapper mapper = new ObjectMapper();
////        
////        //JSON from String to Object
////        Gson gson = new Gson();
////        OjtReflection ojtStudent = gson.fromJson(jsonIn, OjtReflection.class);
////        ojtStudent = cr.save(ojtStudent);
////        String temp = "";
////            temp = gson.toJson(ojtStudent);
////        
////        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
////                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
//
//    }
    

    
    
    
    
}
