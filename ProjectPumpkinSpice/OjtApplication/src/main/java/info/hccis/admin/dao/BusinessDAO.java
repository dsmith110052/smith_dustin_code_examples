package info.hccis.admin.dao;

import info.hccis.camper.model.Business;
import info.hccis.camper.model.DatabaseConnection;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;



public class BusinessDAO {
    
    public static ArrayList<Business> getUsers(DatabaseConnection databaseConnection) {
        ArrayList<Business> businesses = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            sql = "SELECT * FROM `business` order by username";

             ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Business business = new Business();
                business.setId(rs.getInt("id"));
                business.setUsername(rs.getString("username"));
                business.setName(rs.getString("name"));
                business.setPhoneNumber(rs.getString("phoneNumber"));
                business.setWebsite(rs.getString("website"));
                business.setConfirmLetterSentDate(rs.getString("confirmLetterSentDate"));
                business.setLiabilityIdemnificationSentDate(rs.getString("liabilityIdemnificationSentDate"));
                business.setNumberOfPositions(rs.getInt("numberOfPositions"));
                businesses.add(business);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return businesses;
    }
    
    public static void insert()
    {
        Connection conn = null;
       
       String sql = " INSERT INTO business(username,phoneNumber,name) VALUES(?,?,?) ";


        try { 
        BufferedReader bReader = new BufferedReader(new FileReader("/cis2232/businessList.csv"));
        String line = ""; 
        conn = ConnectionUtils.getDBConnection();
        while ((line = bReader.readLine()) != null) {
            try {

                if (line != null) 
                {
                    String[] array = line.split(",+");
                    for(String result:array)
                    {
                        System.out.println(result);
 //Create preparedStatement here and set them and excute them
                PreparedStatement ps = conn.prepareStatement(sql);
                 ps.setString(1,array[0]);
                 ps.setString(2,array[1]);
                 ps.setString(3,array[2]);                 
                 ps.executeUpdate();
                 ps. close();

                    }
                } 
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            finally
            {
               if (bReader == null) 
                {
                    bReader.close();
                }
            }
        }
    } catch (Exception e) {
        e.printStackTrace();
        
    }
    }
  

    
}

