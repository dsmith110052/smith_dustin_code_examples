package info.hccis.admin.dao;

import info.hccis.camper.model.DatabaseConnection;
import info.hccis.camper.model.jpa.CodeType;
import info.hccis.camper.model.jpa.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import info.hccis.camper.model.Student;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


/**
 *
 * @author banderson30742
 */
public class StudentDAO {
    
    public static ArrayList<Student> getUsers(DatabaseConnection databaseConnection) {
        ArrayList<Student> students = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getDBConnection(databaseConnection);

            sql = "SELECT * FROM `student` order by userId";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                  Student student = new Student();
                  student.setStudentId("studentId");
                  student.setFirstName("firstName");
                  student.setFirstName("lastName");
                  student.setPhone("phone");
                  student.setUsername("username");
                  student.setPreferredTypeOfWork("preferredTypeOfWork");
                  student.setCoverLetterSubmittedDate(("coverLetterSubmittedDate"));
                  student.setResumeLocation("resumeSubmittedDate");
                  student.setResumeSubmittedDate("resumeSubmittedDate");
                students.add(student);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return students;
    }
    
    public static void insertCSV()
    {
        Connection conn = null;
       
        try { 
            
        BufferedReader bReader = new BufferedReader(new FileReader("/cis2232/studentList.csv"));
        String line = ""; 
        conn = ConnectionUtils.getDBConnection();
        
        String sql = "INSERT INTO student(studentId,lastName, firstName)"
                + "VALUES (?,?,?)";
        
      
        
        while ((line = bReader.readLine()) != null) {
            try {

                if (line != null) 
                {
                    String[] array = line.split(",+");
                                        
                    //Create preparedStatement here and set them and excute them
                    PreparedStatement ps = conn.prepareStatement(sql);
                    ps.setString(1,array[0]);
                    ps.setString(2,array[1]);
                    ps.setString(3,array[2]);
                    ps.executeUpdate();
                    ps.close();
                    
                    //Assuming that your line from file after split will folllow that sequence

                    
                } 
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally
            {
               if (bReader == null) 
                {
                    bReader.close();
                }
               
            }
        }
    } catch (Exception e) {
        e.printStackTrace();
        
    }
    }
    
}
