package info.hccis.admin.dao;

import info.hccis.camper.model.DatabaseConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionUtils {

    private final static Logger LOGGER = Logger.getLogger(ConnectionUtils.class.getName());

    private static String USER_NAME_DB = "";
    private static String USER_PASSWORD_DB = "";
    private static String DB_NAME = "";
    private static String HOST_NAME = "";

    /**
     * This method will get a connection object based on the database connection
     * properties in the input parameter.
     *
     * @since 20180524
     * @author BJM
     *
     * @param databaseConnection
     * @return connection
     * @throws Exception
     */
    public static Connection getDBConnection(DatabaseConnection databaseConnection) throws Exception {
        String propFileName = "spring.data-access";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        HOST_NAME = rb.getString("jdbc.host");
        try {
            USER_NAME_DB = databaseConnection.getUserName();
            USER_PASSWORD_DB = databaseConnection.getPassword();
            DB_NAME = databaseConnection.getDatabaseName();
        } catch (Exception e) {
            LOGGER.severe("Could not connect using db info - using default from properties file");
        }
        return ConnectionUtils.getDBConnection();
    }

    /**
     * This method will get a connection object based on the properties in the 
     * configuration file 
     *
     * @since 20180524
     * @author BJM
     *
     * @return connection
     * @throws Exception
     */
    public static Connection getDBConnection() throws Exception {
        LOGGER.log(Level.INFO, "in getDBConnection");
        if (USER_NAME_DB.equals("")) {
            String propFileName = "spring.data-access";
            ResourceBundle rb = ResourceBundle.getBundle(propFileName);
            Enumeration<String> keys = rb.getKeys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                String value = rb.getString(key);
                System.out.println(key + ": " + value);
            }
            USER_NAME_DB = rb.getString("jdbc.username");
            USER_PASSWORD_DB = rb.getString("jdbc.password");
            DB_NAME = rb.getString("jdbc.dbname");
            HOST_NAME = rb.getString("jdbc.host");
        }

        Connection conn = null;
        String URL = "jdbc:mysql://" + HOST_NAME + ":3306/" + DB_NAME;
        System.out.println("URL=" + URL);
        System.out.println("User=" + USER_NAME_DB);
        System.out.println("Pw=" + USER_PASSWORD_DB);
        try {
            conn = DriverManager.getConnection(URL, USER_NAME_DB, USER_PASSWORD_DB);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return conn;
    }
    public static Connection getConnection() throws Exception {
        return getDBConnection();
    }

}
