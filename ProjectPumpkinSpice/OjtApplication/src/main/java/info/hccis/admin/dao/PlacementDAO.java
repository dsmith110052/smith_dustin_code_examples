package info.hccis.admin.dao;

import info.hccis.camper.model.Business;
import info.hccis.camper.model.Student;
import info.hccis.camper.model.jpa.Placement;
import info.hccis.camper.model.jpa.PlacementExtended;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * This class gets all data from the database
 *
 * @author erumAteeque
 * @since 20181024
 */
public class PlacementDAO {

    /**
     * Select all Placements and the student name and business name
     *
     * @author erumAteeque
     * @since 20181024
     * @return
     */
    public static ArrayList<PlacementExtended> selectAll() {

        ArrayList<PlacementExtended> placementList = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getDBConnection();

            sql = "SELECT p.id,p.notes,p.placementDate,p.studentId,p.businessId,s.firstName, s.lastName, b.name FROM `placement` p JOIN `student`s ON p.studentId=s.id JOIN `business` b ON p.businessId = b.id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //gettting data from the query
                int id = rs.getInt("id");

                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String notes = rs.getString("notes");
                String placementDate = rs.getString("placementDate");
               // int studentId = rs.getInt("studentId");
                //int businessId = rs.getInt("businessId");

                String businessName = rs.getString("name");

                PlacementExtended extendedContent = new PlacementExtended(id);
                extendedContent.setFirstName(firstName);
                extendedContent.setLastName(lastName);
                extendedContent.setBusinessName(businessName);
                extendedContent.setNotes(notes);
                extendedContent.setPlacementDate(placementDate);
                //extendedContent.setStudentId(studentId);
                //extendedContent.setBusinessId(businessId);

                //extendedContent.setId(id);
                //add the placement object to array list
                placementList.add(extendedContent);
                System.out.println(placementList);

            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return placementList;
    }

    public static ArrayList<Student> selectStudentNotWithPlacement() {

        ArrayList<Student> studentsList = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getDBConnection();

            sql = "SELECT  s.id, s.firstName, s.lastName FROM `student` s WHERE NOT EXISTS (SELECT p.id FROM placement p WHERE s.id = p.studentId)ORDER BY s.firstName";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //gettting data from the query
                int id = rs.getInt("id");

                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");

                Student studentWithNoPlacements = new Student();

                studentWithNoPlacements.setFirstName(firstName);
                studentWithNoPlacements.setLastName(lastName);
                studentWithNoPlacements.setId(id);

                //add the placement object to array list
                studentsList.add(studentWithNoPlacements);
                // System.out.println("\n"+studentsList);

            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return studentsList;
    }

    public static ArrayList<Business> selectBusinessWithNoPlacement() {

        ArrayList<Business> businessesList = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getDBConnection();

            sql = "SELECT b.id, b.name FROM `business` b WHERE NOT EXISTS (SELECT p.id FROM placement p WHERE b.id = p.businessId)ORDER BY b.id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //gettting data from the query
                int id = rs.getInt("id");

                String name = rs.getString("name");
                

                Business businessWithNoPlacements = new Business();

                businessWithNoPlacements.setName(name);               
                businessWithNoPlacements.setId(id);

                //add the placement object to array list
                 businessesList.add(businessWithNoPlacements);
                // System.out.println("\n"+studentsList);

            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return  businessesList;
    }

}
