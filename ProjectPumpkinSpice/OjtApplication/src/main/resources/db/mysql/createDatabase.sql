drop database cis2232_ojt;
create database cis2232_ojt;
use cis2232_ojt;

/*create a user in database*/
grant select, insert, update, delete, alter on cis2232_ojt.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;

drop table if exists Student;
drop table if exists Placement;
drop table if exists Reflection;
drop table if exists CodeValue;
drop table if exists CodeType;
drop table if exists User;

-- create instructor table and set primary key
-- no foreign keys set yet
CREATE TABLE Student (
	id int(5) PRIMARY KEY AUTO_INCREMENT,
        username varchar(100) comment 'Email address',
	phone varchar(15) comment '15 digit phone number',
	studentId 	varchar(50),
	lastName 	varchar(50), 
	firstName 	varchar(20),
        resumeLocation  varchar(100) comment 'online link to resume',
	preferredTypeOfWork varchar(140) comment 'What is the preferred programming field',
	coverLetterSubmittedDate varchar(10) comment 'yyyy-mm-dd',
	resumeSubmittedDate varchar(10) comment 'yyyy-mm-dd'
	);	

CREATE TABLE Business (
	id			int(5) PRIMARY KEY AUTO_INCREMENT,
        username          varchar(100) comment 'Email address',
	name		varchar(50),
	phoneNumber varchar(10) comment '10 digit phone number',
        website varchar(100)  comment 'business website URL',
	confirmLetterSentDate	varchar(10) comment 'yyyy-mm-dd format',
	liabilityIdemnificationSentDate varchar(10) comment 'yyyy-mm-dd format',
	numberOfPositions int(3) default 1 comment 'Number of student positions'
	);


-- no foreign keys set yet	
CREATE TABLE Placement (
	id int(5) PRIMARY KEY AUTO_INCREMENT,
	studentId int(5) comment 'id from the Student table',
	businessId int(5) comment 'id from the Business table',
	placementDate varchar(10) comment 'yyyy-mm-dd format',
        notes varchar(100) comment 'Any notes on the placement'
	);


--
-- Table structure for table CodeType
--

CREATE TABLE CodeType (
  codeTypeId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'This is the primary key for code types',
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  createdDateTime datetime DEFAULT NULL
);

--
-- Table structure for table CodeValue
--

CREATE TABLE CodeValue (
  codeTypeId int(3) NOT NULL COMMENT 'see code_type table',
  codeValueSequence int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  englishDescriptionShort varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  frenchDescriptionShort varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description'
);

--
-- Table structure for table User
--

CREATE TABLE User (
  userId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username varchar(100) NOT NULL COMMENT 'Unique user name for app',
  password varchar(128) NOT NULL,
  lastName varchar(100) NOT NULL,
  firstName varchar(100) NOT NULL,
  userTypeCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  additional1 varchar(100),
  additional2 varchar(100),
  createdDateTime varchar(20) DEFAULT NULL COMMENT 'When user was created.'
);


INSERT INTO CodeType (codeTypeId, englishDescription, frenchDescription) VALUES
(1, 'User Types', 'User Types FR');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort) VALUES
(1, 1, 'Admin', 'Admin', 'AdminFR', 'AdminFR'),
(1, 2, 'Student', 'Student', 'Student', 'Student'),
(1, 3, 'Placement', 'Placement', 'Placement', 'Placement');


INSERT INTO User (userId, username, password, lastName, firstName, userTypeCode, additional1, additional2, createdDateTime) VALUES
(1, 'admin@hccis.info', '202cb962ac59075b964b07152d234b70', 'MacLean', 'BJ', 1, NULL, NULL, sysdate()),
(2, 'student@hccis.info', '202cb962ac59075b964b07152d234b70', 'MacLean', 'BJ', 2, NULL, NULL, sysdate()),
(3, 'placement@hccis.info', '202cb962ac59075b964b07152d234b70', 'MacLean', 'BJ', 3, NULL, NULL, sysdate());

INSERT INTO cis2232_ojt.Student (id, username, phone, studentId, lastName, firstName, resumeLocation, preferredTypeOfWork, coverLetterSubmittedDate, resumeSubmittedDate) VALUES 
('1', 'jsmith@hollandcollege.com', '9023141357', '12345', 'Smith', 'Joe', 'https://www.linkedin.com/in/bruce-bj-maclean-88b40769/', 'Java system programming', NULL, NULL),
('2', 'brichard@hollandcollege.com', '9023142468', '22333', 'Richard', 'Brittany','https://www.linkedin.com/in/bruce-bj-maclean-88b40769/', 'Security', NULL, NULL),
('3', 'galain@hollandcollege.com', '9023145588', '21332', 'Alain', 'Gilles', 'https://www.linkedin.com/in/bruce-bj-maclean-88b40769/', 'Web html css', NULL, NULL),
('4', 'jtownsend@hollandcollege.com', '9025668854', '22569', 'Townsend','Jennifer', NULL, 'General programming', NULL, NULL),
('5', 'caondo@hollandcollege.com', '9025698879', '22855', 'Aondo', 'Charles',NULL, 'Java', NULL, NULL),
('6', 'ljones@hollandcollege.com', '9023145512', '22455', 'Jones', 'Luciano',NULL, 'Web programming', NULL, NULL);

INSERT INTO `cis2232_ojt`.`Business` (`id`, `username`, `name`, `phoneNumber`, `website`, `confirmLetterSentDate`, `liabilityIdemnificationSentDate`, `numberOfPositions`) VALUES 
('1', 'info@maximus.com', 'Maximus Canada', '9025694458', 'https://www.maximuscanada.ca/', NULL, NULL, '2'),
('2', 'info@itss.pe.ca', 'ITSS PEI', '9026282256', 'http://gov.pe.ca', NULL, NULL, '4'),
('3', 'gary@swiftradius.com', 'Swift Radius', '9025661238', 'https://www.swiftradius.com/', NULL, NULL, '1'),
('4', 'joe@superapps.com', 'Super apps', '9028925877', 'https://www.superapps.ca/', NULL, NULL, '1');

INSERT INTO `cis2232_ojt`.`Placement` (`id`, `studentId`, `businessId`, `placementDate`) VALUES 
('1', '2', '1', '20190115'),
('2', '1', '2', '20190115'),
('3', '5', '4', '20190115');
