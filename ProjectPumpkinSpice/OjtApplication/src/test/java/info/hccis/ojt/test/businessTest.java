/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.test;

import info.hccis.camper.model.Business;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class businessTest {
    
    Business business;
    
    public businessTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testSetUsername() {
         
         String username="username";                
         business = new Business();
         business.setUsername(username);
         assertEquals(username, business.getUsername());
     }
    
    @Test
    public void testSetName() {
         
         String name="name";                
         business = new Business();
         business.setName(name);
         assertEquals(name, business.getName());
     }
    
    @Test
    public void testPhoneNumber() {
         
         String phoneNumber="phoneNumber";                
         business = new Business();
         business.setPhoneNumber(phoneNumber);
         assertEquals(phoneNumber, business.getPhoneNumber());
     }
    @Test
    public void testWebsite() {
         
         String website="website";                
         business = new Business();
         business.setWebsite(website);
         assertEquals(website, business.getWebsite());
     }
    @Test
    public void testConfirmLetterSentDate() {
         
         String confirmLetterSentDate="confirmLetterSentDate";                
         business = new Business();
         business.setConfirmLetterSentDate(confirmLetterSentDate);
         assertEquals(confirmLetterSentDate, business.getConfirmLetterSentDate());
     }

}

    