/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.test;

import info.hccis.camper.model.Student;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Testing Getters and Setters 
 * 
 * @author banderson30742
 */
public class studentTest {
    
    Student student;
    
    public studentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testSetUsername() {
         
         String username="test";                
         student = new Student();
         student.setUsername(username);
         assertEquals(username, student.getUsername());
     }
    @Test
    public void testSetPhone() {
         
         String phone="555-5555";
         student = new Student();
         student.setPhone(phone);
         assertEquals(phone, student.getPhone());
     }
    @Test
    public void testSetLastName() {
         
        String lastName="Anderson";                   
         student = new Student();
         student.setLastName(lastName);
         assertEquals(lastName, student.getLastName());
     }
    @Test
    public void testSetFirstName() {
         

         String firstName="Brandon";              
     
         student = new Student();
         student.setFirstName(firstName);
         assertEquals(firstName, student.getFirstName());
     }
    @Test
    public void testSetResumeLocation() {
         

         String resumeLocation="TestLand";

         student = new Student();
         student.setResumeLocation(resumeLocation);
         assertEquals(resumeLocation, student.getResumeLocation());
     }
}

    