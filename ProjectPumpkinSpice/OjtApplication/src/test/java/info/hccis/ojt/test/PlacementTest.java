
package info.hccis.ojt.test;

import info.hccis.camper.model.jpa.Placement;
import info.hccis.camper.model.jpa.PlacementExtended;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author erumAteeque
 * @since 20181202
 */
public class PlacementTest {
    
    Placement placement;
    PlacementExtended placementExtended;
    
    public PlacementTest(){
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
      
    @Test
    public void testSetDate() {         
         String placementDate="2018-11-30";                
          placementExtended= new PlacementExtended();
        placementExtended.setPlacementDate(placementDate);
        assertEquals(placementDate, placementExtended.getPlacementDate());
     }
     @Test
    public void testSetStudentFirstName() {         
         String studentFirstName ="Erum";                
         placementExtended= new PlacementExtended();
        placementExtended.setFirstName(studentFirstName);
        assertEquals(studentFirstName, placementExtended.getFirstName());
     }
    
     @Test
    public void testSetStudentLastName() {         
         String studentLastName ="Ateeque";                
         placementExtended= new PlacementExtended();
        placementExtended.setFirstName(studentLastName);
        assertEquals(studentLastName, placementExtended.getLastName());
     }
    
     @Test
    public void testSetBusinessName() {         
         String businessName ="Maximus Canada";                
         placementExtended= new PlacementExtended();
        placementExtended.setFirstName(businessName);
        assertEquals(businessName, placementExtended.getBusinessName());
     }
     @Test
    public void testSetNotes() {         
         String notes ="Testing Data";                
         placementExtended= new PlacementExtended();
        placementExtended.setNotes(notes);
        assertEquals(notes, placementExtended.getNotes());
     }
}
